package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/elastic/go-elasticsearch"
)

const (
	RequestTimeout               = 5 * time.Second
	ChefkochBaseUrl              = "https://api.chefkoch.de/v2"
	ChefkochRecipeSearchEndpoint = "/search/recipe"
	ChefkochCategoriesEndpoint   = "/recipes/categories"
	ChefkochRecipeDetailEndpoint = "/aggregations/recipe/public/screen-v4/{id}"
)

var DefaultRecipeSearchQueryParams = map[string]string{
	"limit":              "200",
	"offset":             "0",
	"searchIngredients":  "1",
	"searchTitles":       "1",
	"orderBy":            "2",
	"hasImage":           "1",
	"minimumRating":      "3",
	"descendCategories":  "1",
	"searchinstructions": "0",
	"categories":         "2",
	"order":              "0",
}

type RecipeResponse struct {
	Results []struct {
		Score  int `json:"score"`
		Recipe struct {
			ID       string `json:"id"`
			Title    string `json:"title"`
			Subtitle string `json:"subtitle"`
			Rating   struct {
				Rating   float64 `json:"rating"`
				NumVotes int     `json:"numVotes"`
			} `json:"rating"`
			HasImage                bool   `json:"hasImage"`
			SiteUrl                 string `json:"siteUrl"`
			PreviewImageUrlTemplate string `json:"previewImageUrlTemplate"`
		} `json:"recipe"`
	} `json:"results"`
}

func AddQueryParams(request *http.Request, params map[string]string) string {
	queryUrl := request.URL.Query()
	for key, value := range params {
		queryUrl.Add(key, value)
	}
	return queryUrl.Encode()
}

// func DecodeRecipeResponse() (RecipeResponse, error) {

// }

func main() {
	fmt.Println("Starting to scrape data from Chefkoch API")
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		fmt.Printf("elastic search initialization failed %v", err)
		return
	}
	fmt.Println(es)
	ctx, shutdown := context.WithTimeout(context.Background(), RequestTimeout)
	defer shutdown()
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, ChefkochBaseUrl+ChefkochRecipeSearchEndpoint, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	req.URL.RawQuery = AddQueryParams(
		req,
		DefaultRecipeSearchQueryParams,
	)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to request %v", req.URL.String())
		fmt.Println(err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("Failed request with status %d", resp.StatusCode)
		return
	}
	var respRecipeSearch RecipeResponse
	if err := json.NewDecoder(resp.Body).Decode(&respRecipeSearch); err != nil {
		fmt.Println(err)
		return
	}
	if len(respRecipeSearch.Results) > 5 {
		for i := 1; i < 5; i++ {
			fmt.Println(respRecipeSearch.Results[i])
		}
	} else {
		fmt.Println("array not long enough")
		return
	}
}
